# Original credit: https://github.com/jpetazzo/dockvpn

# Leaner build then Ubunutu
FROM debian:jessie

MAINTAINER Yi Tao Jiang <jiangyt.cn@gmail.com>

ADD ./sources.list /etc/apt/
RUN apt-get update && \
    apt-get install -y openvpn iptables git-core nodejs-legacy --fix-missing && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Update checkout to use tags when v3.0 is finally released
RUN git clone --depth 1 --branch v3.0.0-rc2 https://github.com/OpenVPN/easy-rsa.git /usr/local/share/easy-rsa && \
    ln -s /usr/local/share/easy-rsa/easyrsa3/easyrsa /usr/local/bin

# Needed by scripts
ENV OPENVPN /etc/openvpn
ENV EASYRSA /usr/local/share/easy-rsa/easyrsa3
ENV EASYRSA_PKI $OPENVPN/pki
ENV EASYRSA_VARS_FILE $OPENVPN/vars
ENV DEBUG 1

VOLUME ["/etc/openvpn"]

##################
ADD ./bin /usr/local/bin
RUN chmod a+x /usr/local/bin/*

# Add minus user
RUN useradd -m minus

# Add tty.js
ADD ./tty.js /opt/tty.js
ADD ./init.d /etc/init.d/
RUN chmod 755 /etc/init.d/ttyjs
RUN update-rc.d ttyjs defaults
EXPOSE 8080/tcp

#CMD ["ttyjs_run"]
#RUN /usr/local/bin/ttyjs_run &
##################

# Internally uses port 1194/udp, remap using `docker run -p 443:1194/tcp`
EXPOSE 1194/udp

WORKDIR /etc/openvpn
CMD ["box_run"]
